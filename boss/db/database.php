<?php

define('MYSQL_SERVER', 'localhost');
define('MYSQL_USER', 'root');
define('MYSQL_PASSWORD', '');
define('MYSQL_DB', 'calc');

$link = db_connect();

// Return information about UPS model to client
if (isset($_POST['model'])) {
    $result = print_ups_by_model($link, $_POST['model']);
    echo json_encode($result);
}

// DB connection
function db_connect() {
    $link = mysqli_connect(MYSQL_SERVER, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DB) or die("Error: ".mysqli_error($link));
    if(!mysqli_set_charset($link, "utf8"))
        printf("Error: ".mysqli_error($link));
    return $link;
}

// Return result of DB query
function db_query($link, $query) {
    $result = mysqli_query($link, $query);
    return $result;
}

// Print list of orders
function print_orders($link) {
    $result = db_query($link, "SELECT orders.id, orders.name, orders.phone, ups_impl.impl, orders.datetime, orders.comment FROM orders, ups_impl WHERE orders.impl = ups_impl.id ORDER BY id ASC");
    echo '<h1>Заказы</h1><br>';
    if (!$result) 
        echo 'Записи в БД по данному запросу отсутствуют';
    else {
        echo '<table>';
        echo '<tr>
        <th>№</td>
        <th>ФИО</th>
        <th>Телефон</th>
        <th>Цель применения</th>
        <th>Дата и время заказа</th>
        <th>Комментарий</th>
        </tr>';
        for ($i = 0; $i < mysqli_num_rows($result); $i++) {
            $row = mysqli_fetch_assoc($result);
            echo '<tr>';
            echo '<td>'.$row['id'].'</td>';
            echo '<td>'.$row['name'].'</td>';
            echo '<td>'.$row['phone'].'</td>';
            echo '<td>'.$row['impl'].'</td>';
            echo '<td>'.$row['datetime'].'</td>';
            echo '<td>'.$row['comment'].'</td>';
            echo '</tr>';
        }
        echo '</table>';
    }
}

// Fill the select form with list of UPS models
function show_models_select($link) {
    $result = db_query($link, "SELECT * FROM models ORDER BY id ASC");
    echo '<h1>ИБП серии: </h1>';
    echo '<select id="models_field" name="models" onchange="print_ups_by_model()">';
    if ($result) {
        for ($i = 0; $i < mysqli_num_rows($result); $i++) {
            $row = mysqli_fetch_assoc($result);
            echo '<option value='.$row['id'].'>'.$row['model_name'].'</option>';
        }
        echo '</select><br><br>';
    }
}

// Show the UPS list 
function print_ups_by_model($link, $model) {
    $result = db_query($link, "SELECT * FROM ups WHERE model=".mysql_real_escape_string($model)." ORDER BY id ASC");
    if (!$result) 
        echo 'Записи в БД по данному запросу отсутствуют';
    else {
        echo '<table>';
        echo '<tr>
        <th>№</td>
        <th>Наименование</th>
        <th>Артикул</th>
        <th>Описание</th>
        <th>Время автономии, мин</th>
        <th>Размеры, см</th>
        <th>Вес, кг</th>
        <th>Стоимость, $</th>
        </tr>';
        for ($i = 0; $i < mysqli_num_rows($result); $i++) {
            $row = mysqli_fetch_assoc($result);
            echo '<tr>';
            echo '<td>'.$row['id'].'</td>';
            echo '<td>'.$row['ups_name'].'</td>';
            echo '<td>'.$row['article'].'</td>';
            echo '<td>'.$row['description'].'</td>';
            echo '<td>'.$row['time'].'</td>';
            echo '<td>'.$row['dims'].'</td>';
            echo '<td>'.$row['weight'].'</td>';
            echo '<td>'.$row['cost'].'</td>';
            echo '</tr>';
        }
        echo '</table>';
    }
}