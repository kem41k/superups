// User authorization
function authorization() {
    "use strict";

    var msg = $('#login-form').serialize();  
    $.ajax({
        type: 'POST',
        url: 'index.php',               
        data: msg,                      
        success: function(data){
            alert(data);
        },                
        error: function(xhr, str){    
            alert('Возникла ошибка: ' + xhr.responseText);
        }
    });
}

// Show information about chosen UPS
function print_ups_by_model() {
    "use strict";
    
    var selected = parseInt(document.getElementById("models_field").value);
    $.ajax({
        type: 'POST',
        url: 'db/database.php',         
        data: "model=" + selected,           
        success: function(data){
            data = JSON.parse(data);
            alert(data);
        },               
        error: function(xhr, str){              
            alert('Возникла ошибка: ' + xhr.responseText);
        }
    });
}