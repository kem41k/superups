<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
<script src="js/script.js"></script>

<div id="db-table">

    <?php

    // Return data to client
    if (!empty($_GET['table'])) {
        switch ($_GET['table']) {
            case 'orders':
            default:
                print_orders($link);
                break;
            case 'ups':
                show_models_select($link);
                print_ups_by_model($link, '1');
                break;
        }
    }
    else {
        // Default
        print_orders($link);
    }
    ?>
    
</div>