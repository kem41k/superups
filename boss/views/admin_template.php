<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Панель администратора</title>
        <link rel="stylesheet" href="css/reset.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <div id="container">
            <!----------------------------------------- HEADER ----------------------------------------->
            <div id="header">
                Панель администратора
                <?php
                    if (isset($_SESSION['auth'])) 
                        if ($_SESSION['auth'] == 'yes')
                            echo '[<a href="../index.php?logout=1">Выйти</a>]';
                ?>
            </div>
            <!----------------------------------------- CONTENT ----------------------------------------->
            <div id="content">
                <?php
                    if (isset($_SESSION['auth'])) 
                        if ($_SESSION['auth'] == 'yes')
                            echo "[<a href='index.php?table=orders'>Заказы</a>][<a href='index.php?table=ups'>Список ИБП</a>][<a href='../index.php?page=1'>Вернуться на сайт</a>]<hr>";
                    include("views/".$content_page);
                ?>
            </div>
            <!----------------------------------------- FOOTER ----------------------------------------->
            <div id="footer">
                <hr>
                [<a href="../index.php?page=1">Вернуться на сайт</a>]
            </div>
        </div>
    </body>
</html> 