<?php

require("db/database.php");

session_start();

$login_error = '';
$content_page = "login.php";

// If there was no authorization attempts
if (!isset($_SESSION['auth'])) 
    $_SESSION['auth'] = 'no';

// If the user is not authorized
if ($_SESSION['auth'] == 'no') {
    // Show the login form
    if (!isset($_POST['submit']))
        $content_page = "login.php";
    else {
        if ($_POST['submit'] == 'Войти') {
            // Authorization
            $login = trim($_POST['login']);
            $password = trim($_POST['password']);
            if (empty($login) || empty($password)) {
                $login_error = 'Вы не ввели логин и/или пароль!';
                $content_page = "login.php";
            }
            else {
                $result = db_query($link, sprintf("SELECT user_login, user_password FROM users WHERE user_login='%s'", mysql_real_escape_string($login)));

                if (!$result) {
                    $login_error = 'Ошибка аутентификации';
                    $content_page = "login.php";
                }
                else {
                    if (empty(mysqli_num_rows($result))) {
                        $login_error = 'Пользователь '.$login.' не зарегистрирован';
                        $content_page = "login.php";
                    }
                    else {
                        $user = mysqli_fetch_assoc($result);
                        if (md5(md5($password)) == $user['user_password']) {
                            // Remember the user
                            $_SESSION['auth'] = 'yes';
                            $content_page = "admin_page.php";
                        }
                        else {
                            $login_error = 'Неверный пароль!';
                            $content_page = "login.php";
                        }
                    }
                }
            }
        }
    }
}
//Authorization is done
else if ($_SESSION['auth'] == 'yes')
    $content_page = "admin_page.php";

include("views/admin_template.php");