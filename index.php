<?php

session_start();

require("application/db/functions.php");

$link = db_connect();
// Getting lists from DB
$impl_list = read_all_data($link, "ups_impl");
$orders_list = read_all_data($link, "orders");
$models_list = read_all_data($link, "models");

$content_page = "main.php";

if(!empty($_GET['page']))
    switch($_GET['page']){
        case "1":
            $content_page = "main.php";
            break;
        case "2":
            $content_page = "fast.php";
            break;
        case "3":
            $content_page = "engineer.php";
            break;
        case "4":
            $content_page = "detail.php";
            break;
        case "5":
            $content_page = "contacts.php";
            break;
        case "6":
            $content_page = "test.php";
            break;
        default:
            $content_page = "main.php";
            break;
    }

if (!empty($_GET['logout'])) {
    if ($_GET['logout'] == 1) {
        unset($_SESSION['auth']);
        header("Location: index.php");
        exit;
    }
}

// Open the template page with chosen content
include("application/views/template.php");