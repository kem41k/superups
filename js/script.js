var query_result;

// Creation of new order
function new_order() {
    "use strict";
    
    var msg = $('#fast_form').serialize();
    
    if (document.getElementById("impl_field").value != 0) {
        $.ajax({
            type: 'POST',
            url: 'application/db/database.php',     // Server request
            data: msg,                              // Data
            success: function(data){                
                document.getElementById("thank_field").value = 'Спасибо, ' + document.getElementById("name_field").value + '! Ваш заказ принят в обработку!';
                
                // Clear elements
                document.getElementById("name_field").value = '';
                document.getElementById("phone_field").value = '';
                document.getElementById("impl_field").value = 0;
                document.getElementById("comment_field").value = '';
                clear_extra_fields();
            },
            error: function(xhr, str){              // xhr - xmlHttpRequest, str - error type
                alert('Возникла ошибка: ' + xhr.responseText);
                // Очистка элементов формы
                document.getElementById("thank_field").value = '';
                document.getElementById("name_field").value = '';
                document.getElementById("phone_field").value = '';
                document.getElementById("impl_field").value = 0;
                document.getElementById("comment_field").value = '';
                clear_extra_fields();
            }
        });
    }
}

// Dynamical fill of select form
function show_articles() {
    "use strict";
    
    // Clear previous data
    document.getElementById("ups_select_field").innerHTML = '';
    document.getElementById("output1_field").value = '';
    document.getElementById("output2_field").value = '';
    document.getElementById("output3_field").value = '';
    document.getElementById("output4_field").value = '';
    document.getElementById("output5_field").value = '';
    document.getElementById("output6_field").value = '';
    
    var elem = document.getElementById("models_select_field");
    
    $.ajax({
        type: 'POST',
        url: 'application/db/database.php',     
        data: "act=getUPS&model=" + elem.value,       
        success: function(data){      
            // Read answer from server
            data = JSON.parse(data);
            query_result = data;
            // Filling the select form
            var options = '<option value=0 disabled="disabled" selected="selected">Выберите наименование</option>';
            for (var i = 0; i < data.length; i++)
                options += '<option value="' + data[i].id + '">' + data[i].ups_name + '</option>';
            document.forms.test_form.elements.ups_select.innerHTML = options;
        },
        error: function(xhr, str){              // xhr - xmlHttpRequest, str - error type
            alert('Возникла ошибка: ' + xhr.responseText);
        }
    });
}

// Show information about chosen UPS
function show_ups(){
    "use strict";
    
    var chosen_item = document.getElementById("ups_select_field");
    var chosen_ups = chosen_item.options[chosen_item.selectedIndex].text;
    
    for (var i = 0; i < query_result.length; i++)
        if (query_result[i].ups_name == chosen_ups){
            document.getElementById("output1_field").value = 'Артикул: ' + query_result[i].article;
            document.getElementById("output2_field").value = 'Описание: ' + query_result[i].description;
            document.getElementById("output3_field").value = 'Работа при 100% нагрузке: ' + query_result[i].time + ' мин.';
            document.getElementById("output4_field").value = 'Габариты: ' + query_result[i].dims + ' мм';
            document.getElementById("output5_field").value = 'Вес: ' + query_result[i].weight + ' кг';
            document.getElementById("output6_field").value = 'Стоимость: $' + query_result[i].cost;
        }
}

// Show the branch on FastCalc page
function show_branch(){
    "use strict";
    
    clear_extra_fields();
    
    var implementation = document.getElementById("impl_field").value;
    
    switch (implementation) {
        case '1':
        case '2':
        case '4':
        case '5':
        case '6':
        case '7':
            document.getElementById("branch_1").style.display = "block";
            document.getElementById("branch_2").style.display = "none";
            document.getElementById("branch_3").style.display = "none";
            document.getElementById("branch_4").style.display = "none";
            document.getElementById("branch_5").style.display = "none";
            document.getElementById("branch_6").style.display = "none";
            document.getElementById("offer").style.display = "none";
            break;
        case '8':
        case '9':
        case '16':
            document.getElementById("branch_1").style.display = "none";
            document.getElementById("branch_2").style.display = "block";
            document.getElementById("branch_3").style.display = "none";
            document.getElementById("branch_4").style.display = "none";
            document.getElementById("branch_5").style.display = "none";
            document.getElementById("branch_6").style.display = "none";
            document.getElementById("offer").style.display = "none";
            break;
        case '3':
        case '10':
        case '12':
        case '13':
        case '14':
        case '15':
            document.getElementById("branch_1").style.display = "none";
            document.getElementById("branch_2").style.display = "none";
            document.getElementById("branch_3").style.display = "block";
            document.getElementById("branch_4").style.display = "none";
            document.getElementById("branch_5").style.display = "none";
            document.getElementById("branch_6").style.display = "none";
            document.getElementById("offer").style.display = "none";
            break;
        case '11':
        case '16':
        case '17':
        case '18':
        case '19':
        case '20':
            document.getElementById("branch_1").style.display = "none";
            document.getElementById("branch_2").style.display = "none";
            document.getElementById("branch_3").style.display = "none";
            document.getElementById("branch_4").style.display = "block";
            document.getElementById("branch_5").style.display = "none";
            document.getElementById("branch_6").style.display = "none";
            document.getElementById("offer").style.display = "none";
            break;
        case '21':
        case '22':
            document.getElementById("branch_1").style.display = "none";
            document.getElementById("branch_2").style.display = "none";
            document.getElementById("branch_3").style.display = "none";
            document.getElementById("branch_4").style.display = "none";
            document.getElementById("branch_5").style.display = "block";
            document.getElementById("branch_6").style.display = "none";
            document.getElementById("offer").style.display = "none";
            break;
        case '23':
        case '24':
            document.getElementById("branch_1").style.display = "none";
            document.getElementById("branch_2").style.display = "none";
            document.getElementById("branch_3").style.display = "none";
            document.getElementById("branch_4").style.display = "none";
            document.getElementById("branch_5").style.display = "none";
            document.getElementById("branch_6").style.display = "block";
            document.getElementById("offer").style.display = "none";
            break;
        default:
            document.getElementById("branch_1").style.display = "none";
            document.getElementById("branch_2").style.display = "none";
            document.getElementById("branch_3").style.display = "none";
            document.getElementById("branch_4").style.display = "none";
            document.getElementById("branch_5").style.display = "none";
            document.getElementById("branch_6").style.display = "none";
            document.getElementById("offer").style.display = "none";
            break;
    }
}

// Work with slider
function range_change(from_field, to_field) {
    "use strict";
    
    document.getElementById(to_field).value = document.getElementById(from_field).value;
}

// Clear unnecessary data from FastCalc page
function clear_extra_fields() {
    "use strict";
    
    // Branch 1
    document.getElementById("time1_field").value = '60';
    document.getElementById("power1_field").value = '10';
    document.getElementById("range1_time_field").value = 60;
    document.getElementById("range1_power_field").value = 10;
    document.getElementById("radio1_cabinet").checked = true;
    document.getElementById("radio1_wall").checked = false;
    // Branch 2
    document.getElementById("check2_field").checked = false;
    document.getElementById("text2_field").value = '';
    // Branch 3
    document.getElementById("check3_field").checked = false;
    document.getElementById("text3_field").value = '';
    // Branch 4
    document.getElementById("check4_field").checked = false;
    document.getElementById("text4_field").value = '';
    // Branch 5
    document.getElementById("check5_field").checked = false;
    document.getElementById("text5_field").value = '';
    // Branch 6
    document.getElementById("check6_field").checked = false;
    document.getElementById("text6_field").value = '';
    // Results
    document.getElementById("offer").style.display = "none";
}

function choose_from_DB() {
    "use strict";
    
    document.getElementById("offer").style.display = "block";
    
    // Some code here
}