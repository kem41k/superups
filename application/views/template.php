<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="keywords" content="калькулятор, ибп, инвертор">
        <meta name="description" content="Онлайн калькулятор для расчета требуемых ИБП и инверторов под ваш проект!">
        <title>Калькулятор ИБП</title>
        <link rel="stylesheet" href="css/reset.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <div id="container">
            <!----------------------------------------- HEADER ----------------------------------------->
            <div id="header">
                <div id="header-logo">
                    <?php
                        if (!isset($_SESSION['auth']) || $_SESSION['auth'] == 'no') 
                            echo '<a href="index.php?page=1"><img src="images/logo.png"></a>';
                        else
                            if ($_SESSION['auth'] == 'yes') {
                                echo 'Администратор [';
                                echo '<a href="index.php?logout=1">Выйти</a>]';
                            }
                    ?>
                </div>
                <div id="header-menu">
                    <ul>
                        <li><a class="<?=$elem1;?>" href="index.php?page=1">Главная</a></li>
                        <li><a class="<?=$elem2;?>" href="index.php?page=2">Быстрый<br>расчет</a></li>
                        <li><a class="<?=$elem3;?>" href="index.php?page=3">Инженерный<br>расчет</a></li>
                        <li><a class="<?=$elem4;?>" href="index.php?page=4">Детальный<br>расчет</a></li>
                        <li><a class="<?=$elem5;?>" href="index.php?page=5">Контакты</a></li>
                        <li><a class="<?=$elem6;?>" href="index.php?page=6">Тесты</a></li>
                    </ul>
                </div>
            </div>
            <!----------------------------------------- CONTENT ----------------------------------------->
            <div id="content">
            <?php
                include("application/views/".$content_page)
            ?>
            </div>
            <!----------------------------------------- FOOTER ----------------------------------------->
            <div id="footer">
            <br><hr>
                <p align="center"><img src="images/encopro_logo.png"></p>
            </div>
        </div>
    </body>
</html> 