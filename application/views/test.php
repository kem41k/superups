<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<script src="js/script.js"></script>

<div>
    Вывод информации по конкретному ИБП
</div>
<form method="POST" id="test_form" action="">
    <div>
        <select id="models_select_field" name="models_select" onchange="show_articles()">
            <option value="0" disabled="disabled" selected="selected">Выберите серию</option>
            <?php foreach($models_list as $model): ?>
                <option value="<?=$model['id']?>">
                    <?=$model['model_name']?>
                </option>
            <?php endforeach ?>
        </select>
    </div>
    <div>
        <select id="ups_select_field" name="ups_select" onchange="show_ups()">
            <option value="0" disabled="disabled" selected="selected">Выберите наименование</option>
        </select>
    </div>
    <br>
    <div>
        <output type="text" id="output1_field" name="output1"></output><br>
        <output type="text" id="output2_field" name="output2"></output><br>
        <output type="text" id="output3_field" name="output3"></output><br>
        <output type="text" id="output4_field" name="output4"></output><br>
        <output type="text" id="output5_field" name="output5"></output><br>
        <output type="text" id="output6_field" name="output6"></output><br>
    </div>
</form>
<br>
<hr>
<br>
<div>
    Список всех заказов
</div>
<div>
    <table border="1" align="center">
        <tr align="center">
            <th>№</th>
            <th>ФИО клиента</th>
            <th>Телефон</th>
            <th>Цель применения ИБП</th>
            <th>Время и дата</th>
            <th>Комментарий</th>
        </tr>
        <?php foreach($orders_list as $order): ?>
        <tr align="center">
            <td><?=$order['id']?></td>
            <td><?=$order['name']?></td>
            <td><?=$order['phone']?></td>
            <td><?=$order['impl']?></td>
            <td><?=$order['datetime']?></td>
            <td><?=$order['comment']?></td>
        </tr>
        <?php endforeach ?>
    </table>
</div>
<br>