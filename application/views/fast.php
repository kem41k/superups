<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<script src="js/script.js"></script>
<script src="js/jquery.maskedinput.js"></script>

<!-- Left container -->
<div id="content-left">
    <form method="POST" id="fast_form" action="javascript:void(null);" onsubmit="new_order()">
        <label for="impl">
            <h3>Цель применения ИБП</h3>
            <p>
                <select style="width:100%" id="impl_field" name="impl" onchange="show_branch()" required>
                    <option style="width:300px" value="0" disabled="disabled" selected="selected">Выберите вариант</option>
                    <?php foreach($impl_list as $impl): ?>
                        <option style="width:300px" value="<?=$impl['id']?>">
                            <?=$impl['impl']?>
                        </option>
                    <?php endforeach ?>
                </select>
            </p>
        </label>
        <!-- Branch №1 -->
        <div id="branch_1" style="display: none">
            <hr>
            <h3>Дополнит. параметры</h3><br>
            Время автономии: <output type="text" id="time1_field">60</output> мин.<br>
            10&nbsp;<input type="range" id="range1_time_field" name="range1_time" min="10" max="300" step="10" value="60" width="80%" oninput='range_change("range1_time_field", "time1_field")'>&nbsp;300<br><br>
            Мощность: <output type="text" id="power1_field">10</output> кВА.<br>
            1&nbsp;<input type="range" id="range1_power_field" name="range1_power" min="1" max="30" step="1" value="10" width="80%" oninput='range_change("range1_power_field", "power1_field")'>&nbsp;30<br><br>
            Монтаж:<br>
            <label for="radio1_cabinet"><input type="radio" id="radio1_cabinet" name="radio1" value="r1_cabinet" checked>в шкафу<br></label>
            <label for="radio1_wall"><input type="radio" id="radio1_wall" name="radio1" value="r1_wall">на полке<br></label>
            <br>
            <input type="button" id="choose1_btn" name="choose1" onclick="choose1_from_DB()" value="Подобрать варианты">
        </div>
        <!-- Branch №2 -->
        <div id="branch_2" style="display: none">
            <br>
            <h3>Дополнительные параметры (2)</h3><br>
            <input type="checkbox" id="check2_field" name="check2">Выбор 2<br>
            <input type="text" id="text2_field" name="text2">
            <br>
            <input type="button" id="choose2_btn" name="choose2" onclick="choose2_from_DB()" value="Подобрать варианты">
        </div>
        <!-- Branch №3 -->
        <div id="branch_3" style="display: none">
            <br>
            <h3>Дополнительные параметры (3)</h3><br>
            <input type="checkbox" id="check3_field" name="check3">Выбор 3<br>
            <input type="text" id="text3_field" name="text3">
            <br>
            <input type="button" id="choose3_btn" name="choose3" onclick="choose3_from_DB()" value="Подобрать варианты">
        </div>
        <!-- Branch №4 -->
        <div id="branch_4" style="display: none">
            <br>
            <h3>Дополнительные параметры (4)</h3><br>
            <input type="checkbox" id="check4_field" name="check4">Выбор 4<br>
            <input type="text" id="text4_field" name="text4">
            <br>
            <input type="button" id="choose4_btn" name="choose4" onclick="choose4_from_DB()" value="Подобрать варианты">
        </div>
        <!-- Branch №5 -->
        <div id="branch_5" style="display: none">
            <br>
            <h3>Дополнительные параметры (5)</h3><br>
            <input type="checkbox" id="check5_field" name="check5">Выбор 5<br>
            <input type="text" id="text5_field" name="text5">
            <br>
            <input type="button" id="choose5_btn" name="choose5" onclick="choose5_from_DB()" value="Подобрать варианты">
        </div>
        <!-- Branch №6 -->
        <div id="branch_6" style="display: none">
            <br>
            <h3>Дополнительные параметры (6)</h3><br>
            <input type="checkbox" id="check6_field" name="check6">Выбор 6<br>
            <input type="text" id="text6_field" name="text6">
            <br>
            <input type="button" id="choose6_btn" name="choose6" onclick="choose6_from_DB()" value="Подобрать варианты">
        </div>
    </form>
</div>
<!-- Right container -->
<div id="content-right">
    <div id="offer" style="display: none">
        Здесь появятся предлагаемые предложения из БД.<br>
        ...<br>
        ...<br>
        ...<br>
        ...<br>
        ...<br>
        Понравилось предложение? Тогда заполните форму для получения результатов<br>
        <br>
        <div id="pers-form">
            <table id="pers-table">
                <tr>
                    <label for="name">
                        <td class="td-right">
                            ФИО
                        </td>
                        <td>
                            <input type="text" id="name_field" name="name" size="30" autofocus required>
                        </td>
                    </label>
                </tr>
                <tr>
                    <label for="phone">
                        <td class="td-right">
                            Телефон
                        </td>
                        <td>
                            <input type="text" id="phone_field" name="phone" size="30" required>
                            <script type="text/javascript">
                                $(function(){
                                  $("#phone_field").mask("+7(999)999-99-99");
                                });
                            </script>
                        </td>
                    </label>
                </tr>
                <tr>
                    <label for="email">
                        <td class="td-right">
                            e-mail
                        </td>
                        <td>
                            <input type="text" id="email_field" name="email" size="30" required>
                        </td>
                    </label>
                </tr>
                <tr>
                    <label for="comment">
                        <td class="td-right">
                            Комментарии<br>
                            (500 симв.)
                        </td>
                        <td>
                            <textarea id="comment_field" name="comment" cols="32" rows="4" maxlength="500"></textarea>
                        </td>
                    </label>
                </tr>
            </table>
            <p align="center"><input type="submit" value="Выслать предложение на почтовый ящик"><br></p>  
        </div>
        <p class="p-thank"><output type="text" id="thank_field" name="thank"></output></p>
    </div>
</div>