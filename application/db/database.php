<?php
    require_once("functions.php");

    // Add new order to DB
    if(!empty($_POST['name']) && !empty($_POST['phone']) && !empty($_POST['impl'])){
        $link = db_connect();
        add_order($link, $_POST['name'], $_POST['phone'], $_POST['impl'], $_POST['comment']);  
    } 
    // Read data from DB for selected UPS
    else if(!empty($_POST['act']) && !empty($_POST['model'])){
        $link = db_connect();
        $result = read_ups_with_model($link, $_POST['model']);
        echo json_encode($result);
    }