<?php
    define('MYSQL_SERVER', 'localhost');
    define('MYSQL_USER', 'root');
    define('MYSQL_PASSWORD', ''); 
    define('MYSQL_DB', 'calc');

    // DB connection
    function db_connect(){
        $link = mysqli_connect(MYSQL_SERVER, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DB) or die("Error: ".mysqli_error($link));
        if(!mysqli_set_charset($link, "utf8")){
            printf("Error: ".mysqli_error($link));
        }
        return $link;
    }
    
    // Read data from table
    function read_all_data($link, $table_name){
        $query = "SELECT * FROM ".$table_name." ORDER BY id ASC";
        $result = mysqli_query($link, $query);
        
        if(!$result)
            die(mysqli_error($link));
        
        // Result
        $data = array();    
    
        for($i = 0; $i < mysqli_num_rows($result); $i++){
            $row = mysqli_fetch_assoc($result);
            $data[] = $row; 
        }
    
        return $data;
    }

    // Read information about UPS
    function read_ups_with_model($link, $model){
        $query = "SELECT * FROM ups WHERE model=".$model." ORDER BY id ASC";
        $result = mysqli_query($link, $query);
        
        if(!$result)
            die(mysqli_error($link));
        
        // Results
        $data = array();    
    
        for($i = 0; $i < mysqli_num_rows($result); $i++){
            $row = mysqli_fetch_assoc($result);
            $data[] = $row; 
        }
    
        return $data;
    }

    // Addition of new order to DB
    function add_order($link, $name, $phone, $type, $comment){
        $name = trim($name);
        $phone = trim($phone);
        $type = (int)$type;
        $comment = trim($comment);
        
        if($name == "" || $phone == "" || $type < 1)
            return false;

        $datetime = date('d-m-Y H:i:s', time());
        
        // Query for new order addition
        $query = sprintf("INSERT INTO orders (name, phone, impl, datetime, comment) VALUES ('%s', '%s', '%d', '%s', '%s')", mysqli_real_escape_string($link, $name), mysqli_real_escape_string($link, $phone), $type, mysqli_real_escape_string($link, $datetime), mysqli_real_escape_string($link, $comment));  
        
        $result = mysqli_query($link, $query);
        if (!$result)
            die(mysqli_error($link));
        
        return true;
    }